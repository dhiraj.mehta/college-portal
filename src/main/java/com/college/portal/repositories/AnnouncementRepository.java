package com.college.portal.repositories;

import com.college.portal.models.Announcement;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AnnouncementRepository extends PagingAndSortingRepository<Announcement, Long> {
}
