package com.college.portal.repositories;

import com.college.portal.models.RailwayConcession;
import org.springframework.data.repository.CrudRepository;

public interface RailwayConcessionRepository extends CrudRepository<RailwayConcession, Long> {
}
