package com.college.portal.controllers.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class DemoController {

    @GetMapping("/demo")
    public ResponseEntity<Map> home(){
        Map<String, String> jsonObject = new HashMap<>();
        jsonObject.put("demo", "this is demo controller");
        return new ResponseEntity<Map>(jsonObject, HttpStatus.OK);
    }

}
