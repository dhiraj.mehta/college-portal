package com.college.portal.enums;

public enum DepartmentEnum {
    IT,
    CE,
    ME,
    EXTC,
    BCOM,
    BFM,
    BIO_TECH,
    MBBS,
    HR
}
