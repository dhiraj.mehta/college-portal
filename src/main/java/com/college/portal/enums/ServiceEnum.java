package com.college.portal.enums;

public enum ServiceEnum {
    ATTENDANCE,
    RAILWAY_CONCESSION,
    ANNOUNCEMENT;

    public static final double EMPTY_PRICE = 0;

    public double getPrice(){
        switch(this){
            case ATTENDANCE: return 300.00;
            case ANNOUNCEMENT: return 200.00;
            case RAILWAY_CONCESSION: return 250.00;
        }
        return 0;
    }
}
