package com.college.portal.enums;

public enum ParentDepartmentEnum {
    COMMERCE,
    ENGINEERING,
    MBA,
    PHARMACY
}
