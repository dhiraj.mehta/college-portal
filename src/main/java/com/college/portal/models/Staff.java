package com.college.portal.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Staff extends Person {
    private String prn;
    private String role;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "staff")
    private Set<Address> addresses;

    @ManyToOne
    private CollegeDepartment collegeDepartment;

    @OneToMany(mappedBy = "staff")
    private Set<Announcement> announcements;

    @ManyToOne
    private College college;

    public Staff() {
    }

    public Staff(String firstName,
                 String lastName,
                 String email,
                 College college,
                 String prn,
                 String role,
                 Set<Address> addresses,
                 CollegeDepartment collegeDepartment,
                 Set<Announcement> announcements) {
        super(firstName, lastName, email);
        this.prn = prn;
        this.role = role;
        this.addresses = addresses;
        this.collegeDepartment = collegeDepartment;
        this.announcements = announcements;
        this.college = college;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public CollegeDepartment getCollegeDepartment() {
        return collegeDepartment;
    }

    public void setCollegeDepartment(CollegeDepartment collegeDepartment) {
        this.collegeDepartment = collegeDepartment;
    }

    public Set<Announcement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(Set<Announcement> announcements) {
        this.announcements = announcements;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    @Override
    public String toString() {
        return "Staff{" +
                "prn='" + prn + '\'' +
                ", role='" + role + '\'' +
                ", addresses=" + addresses +
                ", collegeDepartments=" + collegeDepartment+
                ", announcements=" + announcements +
                '}';
    }
}
