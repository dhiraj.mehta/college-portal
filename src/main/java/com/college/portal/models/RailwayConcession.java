package com.college.portal.models;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class RailwayConcession extends BaseEntity {
    private String source;
    private String destination;
    private String validityPeriod;
    private LocalDate applyDate;
    private LocalDate expireDate;

    @ManyToOne
    private Student student;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(String validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public LocalDate getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(LocalDate applyDate) {
        this.applyDate = applyDate;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public String toString() {
        return "RailwayConcession{" +
                "source='" + source + '\'' +
                ", destination='" + destination + '\'' +
                ", validityPeriod='" + validityPeriod + '\'' +
                ", applyDate=" + applyDate +
                ", expireDate=" + expireDate +
                ", student=" + student +
                '}';
    }
}
