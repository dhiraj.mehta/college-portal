package com.college.portal.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class College extends BaseEntity{
    private int collegeCode;
    private String collegeName;
    private String collegeDomain;

    @OneToOne(cascade = CascadeType.ALL)
    private Address collegeAddress;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "college")
    private Set<Student> students;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "college")
    private Set<Teacher> teachers;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "college")
    private Set<CollegeDepartment> collegeDepartments;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "college")
    private Set<Staff> staff;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "college")
    private Set<Subscription> subscriptions;

    public int getCollegeCode() {
        return collegeCode;
    }

    public void setCollegeCode(int collegeCode) {
        this.collegeCode = collegeCode;
    }

    public String getCollegeName() {
        return collegeName;
    }

    public void setCollegeName(String collegeName) {
        this.collegeName = collegeName;
    }

    public String getCollegeDomain() {
        return collegeDomain;
    }

    public void setCollegeDomain(String collegeDomain) {
        this.collegeDomain = collegeDomain;
    }

    public Address getCollegeAddress() {
        return collegeAddress;
    }

    public void setCollegeAddress(Address collegeAddress) {
        this.collegeAddress = collegeAddress;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Set<CollegeDepartment> getCollegeDepartments() {
        return collegeDepartments;
    }

    public void setCollegeDepartments(Set<CollegeDepartment> collegeDepartments) {
        this.collegeDepartments = collegeDepartments;
    }

    public Set<Staff> getStaff() {
        return staff;
    }

    public void setStaff(Set<Staff> staff) {
        this.staff = staff;
    }

    @Override
    public String toString() {
        return "College{" +
                "collegeCode=" + collegeCode +
                ", collegeName='" + collegeName + '\'' +
                ", collegeDomain='" + collegeDomain + '\'' +
                ", collegeAddress=" + collegeAddress +
                ", students=" + students +
                ", teachers=" + teachers +
                ", collegeDepartments=" + collegeDepartments +
                ", staff=" + staff +
                '}';
    }
}
