package com.college.portal.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Course extends BaseEntity{
    private String courseCode;
    private String courseName;

    @ManyToOne
    private Department department;

    @ManyToOne
    private Semester semester;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "course")
    private Set<Attendance> attendances;

    @ManyToMany(mappedBy = "courses")
    private Set<Student> student;

    @ManyToMany(mappedBy = "courses")
    private Set<Teacher> teachers;

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Semester getSemester() {
        return semester;
    }

    public void setSemester(Semester semester) {
        this.semester = semester;
    }

    public Set<Attendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(Set<Attendance> attendances) {
        this.attendances = attendances;
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseCode='" + courseCode + '\'' +
                ", courseName='" + courseName + '\'' +
                ", department=" + department +
                ", semester=" + semester +
                '}';
    }
}
