package com.college.portal.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SubscriptionCompositeKey  implements Serializable {
    @Column(name = "college_id")
    private Long collegeId;

    @Column(name = "service_id")
    private Long serviceId;

    public SubscriptionCompositeKey() {
    }

    public SubscriptionCompositeKey(Long collegeId, Long serviceId) {
        this.collegeId = collegeId;
        this.serviceId = serviceId;
    }

    public Long getCollegeId() {
        return collegeId;
    }

    public void setCollegeId(Long collegeId) {
        this.collegeId = collegeId;
    }

    public Long getServiceId() {
        return serviceId;
    }

    public void setServiceId(Long serviceId) {
        this.serviceId = serviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionCompositeKey that = (SubscriptionCompositeKey) o;
        return collegeId.equals(that.collegeId) &&
                serviceId.equals(that.serviceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(collegeId, serviceId);
    }

    @Override
    public String toString() {
        return "SubscriptionCompositeKey{" +
                "collegeId=" + collegeId +
                ", serviceId=" + serviceId +
                '}';
    }
}
