package com.college.portal.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Semester extends  BaseEntity{
    private String semesterName;

    @OneToMany(mappedBy = "semester")
    private Set<Course> courses;

    public String getSemesterName() {
        return semesterName;
    }

    public void setSemesterName(String semesterName) {
        this.semesterName = semesterName;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "Semester{" +
                "semesterName='" + semesterName + '\'' +
                ", courses=" + courses +
                '}';
    }
}
