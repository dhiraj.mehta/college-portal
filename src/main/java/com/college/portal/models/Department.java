package com.college.portal.models;

import com.college.portal.enums.DepartmentEnum;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Department extends BaseEntity{
    @Enumerated(value = EnumType.STRING)
    private DepartmentEnum departmentName;

    @ManyToOne
    private ParentDepartment parentDepartment;

    @OneToMany(mappedBy = "department")
    private Set<Student> students;

    @OneToMany(cascade = CascadeType.DETACH, mappedBy = "department")
    private Set<Teacher> teachers;

    @OneToMany(mappedBy = "department")
    private Set<Course> courses;

    public DepartmentEnum getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(DepartmentEnum departmentName) {
        this.departmentName = departmentName;
    }

    public ParentDepartment getParentDepartment() {
        return parentDepartment;
    }

    public void setParentDepartment(ParentDepartment parentDepartment) {
        this.parentDepartment = parentDepartment;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", parentDepartment=" + parentDepartment +
                ", students=" + students +
                ", teachers=" + teachers +
                ", courses=" + courses +
                '}';
    }
}
