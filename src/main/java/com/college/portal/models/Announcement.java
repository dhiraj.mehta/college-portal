package com.college.portal.models;

import javax.persistence.*;

@Entity
public class Announcement extends BaseEntity{
    private String title;

    @Lob // Use to store characters more than 255;
    private String body;

    @Lob
    private String receivers;

    private String attachments;

    @ManyToOne
    private Staff staff;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getReceivers() {
        return receivers;
    }

    public void setReceivers(String receivers) {
        this.receivers = receivers;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    @Override
    public String toString() {
        return "Announcement{" +
                "title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", receivers='" + receivers + '\'' +
                ", attachments='" + attachments + '\'' +
                ", staff=" + staff +
                '}';
    }
}

