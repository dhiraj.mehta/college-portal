package com.college.portal.models;

import javax.persistence.*;

@Entity
public class Subscription{
    @EmbeddedId
    private SubscriptionCompositeKey id;

    @ManyToOne
    @MapsId("collegeId")
    @JoinColumn(name = "college_id")
    private College college;

    @ManyToOne
    @MapsId("serviceId")
    @JoinColumn(name = "service_id")
    private Service service;

    private String paymentMode;
    private float price;

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                ", paymentMode='" + paymentMode + '\'' +
                ", price=" + price +
                '}';
    }

}
