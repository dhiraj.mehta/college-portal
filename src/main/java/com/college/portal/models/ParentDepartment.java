package com.college.portal.models;

import com.college.portal.enums.ParentDepartmentEnum;

import javax.persistence.*;
import java.util.Set;

@Entity
public class ParentDepartment extends BaseEntity{
    @Enumerated(value = EnumType.STRING)
    private ParentDepartmentEnum parentDepartmentName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentDepartment")
    private Set<Department> departments;

    public ParentDepartmentEnum getParentDepartmentName() {
        return parentDepartmentName;
    }

    public void setParentDepartmentName(ParentDepartmentEnum parentDepartmentName) {
        this.parentDepartmentName = parentDepartmentName;
    }

    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }

    @Override
    public String toString() {
        return "ParentDepartment{" +
                "parentDepartmentName='" + parentDepartmentName + '\'' +
                ", departments=" + departments +
                '}';
    }
}
