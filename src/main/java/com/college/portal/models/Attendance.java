package com.college.portal.models;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
public class Attendance extends BaseEntity{
    @ManyToOne
    private Student student;

    @ManyToOne
    private Course course;

    private LocalDate date;
    private DayOfWeek day;
    private LocalTime time;
    private Boolean status;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DayOfWeek getDay() {
        return day;
    }

    public void setDay(DayOfWeek day) {
        this.day = day;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Attendance{" +
                ", studentId=" + student +
                ", courseId=" + course +
                ", date=" + date +
                ", day=" + day +
                ", time=" + time +
                ", status=" + status +
                '}';
    }

}
