package com.college.portal.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Teacher extends Person {
    private String qualification;
    private String designation;
    private String role;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teacher")
    private Set<Address> addresses;

    @ManyToOne
    private Department department;

    @ManyToOne
    private College college;

    @ManyToMany
    @JoinTable(
            name = "Teaching",
            joinColumns = @JoinColumn(name = "teacher_primary_key"),
            inverseJoinColumns = @JoinColumn(name = "course_primary_key")
    )
    private Set<Course> courses;

    public Teacher() {
    }

    public Teacher(String firstName,
                   String lastName,
                   String email,
                   College college,
                   String qualification,
                   String designation,
                   String role,
                   Set<Address> addresses,
                   Department department) {
        super(firstName, lastName, email);
        this.qualification = qualification;
        this.designation = designation;
        this.role = role;
        this.addresses = addresses;
        this.department = department;
        this.college = college;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "qualification='" + qualification + '\'' +
                ", designation='" + designation + '\'' +
                ", role='" + role + '\'' +
                ", addresses=" + addresses +
                ", department=" + department +
                ", college=" + college +
                '}';
    }
}
