package com.college.portal.models;

import com.college.portal.enums.CollegeDepartmentEnum;

import javax.persistence.*;
import java.util.Set;

@Entity
public class CollegeDepartment extends BaseEntity{
    @Enumerated(value = EnumType.STRING)
    private CollegeDepartmentEnum collegeDepartmentName;

    @OneToMany(mappedBy = "collegeDepartment")
    private Set<Staff> staffs;

    @ManyToOne
    private College college;

    public CollegeDepartmentEnum getCollegeDepartmentName() {
        return collegeDepartmentName;
    }

    public void setCollegeDepartmentName(CollegeDepartmentEnum collegeDepartmentName) {
        this.collegeDepartmentName = collegeDepartmentName;
    }

    public Set<Staff> getStaffs() {
        return staffs;
    }

    public void setStaffs(Set<Staff> staffs) {
        this.staffs = staffs;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    @Override
    public String toString() {
        return "CollegeDepartment{" +
                "collegeDepartmentName='" + collegeDepartmentName + '\'' +
                ", staff=" + staffs +
                ", college=" + college +
                '}';
    }
}
