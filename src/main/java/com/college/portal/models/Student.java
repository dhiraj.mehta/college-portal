package com.college.portal.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Student extends Person{
    private String prn;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private Set<Address> addresses;

    @ManyToOne
    private Department department;

    @ManyToOne
    private College college;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private Set<RailwayConcession> railwayConcessions;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private Set<Attendance> attendances;

    @ManyToMany
    @JoinTable(
        name = "Enrolled",
        joinColumns = @JoinColumn(name = "student_primary_key"),
        inverseJoinColumns = @JoinColumn(name = "course_primary_key")
    )
    private Set<Course> courses;

    public Student() {
    }

    public Student(String firstName,
                   String lastName,
                   String email,
                   String prn,
                   Set<Address> addresses,
                   Department department,
                   College college,
                   Set<RailwayConcession> railwayConcessions,
                   Set<Attendance> attendances) {
        super(firstName, lastName, email);
        this.prn = prn;
        this.addresses = addresses;
        this.department = department;
        this.college = college;
        this.railwayConcessions = railwayConcessions;
        this.attendances = attendances;
    }

    public String getPrn() {
        return prn;
    }

    public void setPrn(String prn) {
        this.prn = prn;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public College getCollege() {
        return college;
    }

    public void setCollege(College college) {
        this.college = college;
    }

    public Set<RailwayConcession> getRailwayConcessions() {
        return railwayConcessions;
    }

    public Set<Attendance> getAttendances() {
        return attendances;
    }

    public void setAttendances(Set<Attendance> attendances) {
        this.attendances = attendances;
    }

    public void setRailwayConcessions(Set<RailwayConcession> railwayConcessions) {
        this.railwayConcessions = railwayConcessions;
    }

    @Override
    public String toString() {
        return "Student{" +
                "prn='" + prn + '\'' +
                ", addresses=" + addresses +
                ", department=" + department +
                ", college=" + college +
                ", railwayConcessions=" + railwayConcessions +
                '}';
    }
}
