package com.college.portal.models;

import com.college.portal.enums.ServiceEnum;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Service extends BaseEntity{
    @Enumerated(value = EnumType.STRING)
    private ServiceEnum serviceName;

    private double price;
    private int discount;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "service")
    private Set<Subscription> subscriptions;

    public ServiceEnum getServiceName() {
        return serviceName;
    }

    public void setServiceName(ServiceEnum serviceName) {
        this.serviceName = serviceName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Service{" +
                ", serviceName='" + serviceName + '\'' +
                ", price=" + price +
                ", discount=" + discount +
                '}';
    }

}
